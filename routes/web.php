<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', function () {
    return view('home');
});
Route::get('/galeryitem', function () {
    return view('item.GaleryItem');
});


Route::get('/itemGallery', 'GaleryItemController@index');
Route::get('/itemGallery/{id}', 'GaleryItemController@showedit');

Route::delete('deleteAll/supplier', 'suplierController@deleteAll');
Route::delete('deleteAll/item', 'itemController@deleteAll');


Route::get('/supplierGallery', 'supplierGalleryController@index');
Route::get('/supplierGallery/{id}', 'supplierGalleryController@showedit');
Route::get('/supplierGallery/{id}/edit', 'supplierGalleryController@edit');
Route::put('/supplierGallery/{id}', 'supplierGalleryController@update');
Route::delete('/supplierGallery/destroy/{id}', 'supplierGalleryController@destroy')->middleware('cors');

//cobain
Route::get('/testing', function(){
  return view('admin.admin_login');
});

//Admin Route Middleware
Route::match(['get', 'post'], '/admin', 'adminController@login');


Route::group(['middleware' => ['auth']],function(){

  //Admin Routes
  Route::get('/admin/dashboard','adminController@dashboard');
  Route::get('/logout','adminController@logout');
  Route::get('/admin/settings','adminController@settings');
  Route::resource('/admin/suplier', 'suplierController');
  Route::resource('/admin/item', 'itemController');
});
