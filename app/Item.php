<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['id','nama', 'image','code','deskripsi', 'qty'];
    public function scopeSearch($query, $s) {
    	return $query->where('nama', 'like', '%' .$s. '%')
    	->orWhere('deskripsi', 'like', '%' .$s. '%');
    }
}
