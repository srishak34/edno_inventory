<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use DB;
use Session;
use Storage;
use Image;
use File;
use Validator;

class itemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request -> input('search');
        $items = item::orderBy('id', 'asc')      
        ->search($search)
        ->paginate(3);

        return view('/item/itemView', compact('items', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this -> validate($request, [
        //     'nama' => 'required',
        //     // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1000',
        //     'code' => 'required',
        //     'deskripsi' => 'required',
        //     'qty' => 'required'

        // ]);
        // $uploadedImage = $request->file('image');

        // $path = $this->uploadImage($request,$uploadedImage);
            $uploadedImage = $request->file('image');
            if ($uploadedImage !== NULL) {
                $path = $this->uploadImage($request ,$uploadedImage);
            } else {
                $path = str_replace('/storage/', 'public/', $request -> input('defaultimage'));

            }


            $items = new item;

            $items -> nama = $request -> nama ;
            $items -> image = $path;
            $items -> code = $request -> code ;
            $items -> deskripsi = $request -> deskripsi ;
            $items -> qty = $request -> qty ;

            $items -> save();

            Session::flash('success', 'Item Has Been Successfully Added.');

            return redirect()->route('item.index');
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Item::findOrFail($request -> id_item);

      $uploadedImage= $request->file('imageedit');

      if($uploadedImage !== NULL)
      {
        $old = str_replace('/storage', '', $request->image_old);
        $nama_file=Storage::url($old);
        if(File::exists('.'.$nama_file))
        {
            File::delete('.'.$nama_file);
        }
        $path   = $this->uploadImage($request ,$uploadedImage);
    }
    else{
        $path = str_replace('/storage/', 'public/', $request->image_old);
        
    }
    $update -> nama = $request -> nama ;
    $update -> image= $path;
    $update -> deskripsi = $request -> deskripsi ;
    $update -> code = $request -> code ;
    $update -> qty = $request -> qty ;

    $update -> save();


    $id = $request -> name;

            // Session::flash('u_success','item ' . $id . ' Has Been Updated.');

    return back();

}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = item::find($id);
        $defaultimage = "".$data -> image;

        switch ($defaultimage) {
            case 'public/no-image.jpg':
            $data -> delete();
            break;
            
            default:
            Storage::delete($data->image);
            $data -> delete();
            break;
        }


        return redirect()->route('item.index');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        item::whereIn('id',explode(",",$ids))
        ->delete();
        return response()->json(['success'=>"Products Deleted successfully."]);
    }
    function uploadImage($request, $image){

       $ext = '.'.$image->getClientOriginalExtension();
       $nama_file = $request -> input('nama').$ext;

       $img = Image::make($image->getRealPath());

       $img->stream();

       $uploaded = Storage::disk('local')->put('public/item'.'/'.$nama_file, $img, 'public');

       $path = '';
       if($uploaded){
        $path = 'public/item/'.$nama_file;
    }
    return $path;
}
}
