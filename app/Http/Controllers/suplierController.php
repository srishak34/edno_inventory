<?php

namespace App\Http\Controllers;

use App\Supplier;
use DB;
use Illuminate\Http\Request;
use Session;
use Storage;
use Image;
use File;

class suplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request -> input('search');
        $supplier = Supplier::orderBy('id', 'asc')
        ->search($search)
        ->paginate(3);

        return view('/suplier/suplierView', compact('supplier', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this -> validate($request, [
        //     'name' => 'required',
        //     'image' => 'required',
        //     'address' => 'required',
        //     'code' => 'required',
        //     'region' => 'required',
        //     'city' => 'required',
        //     'country' => 'required',
        //     'number' => 'required',
        //     'email' => 'required',
        //     'c_title' => 'required',
        //     'c_name' => 'required',
        //     'fax' => 'required'
        // ]);


        $uploadedImage = $request->file('image');
        if ($uploadedImage !== NULL) {
            $path = $this->uploadImage($request ,$uploadedImage);
        } else {
            $path = str_replace('/storage/', 'public/', $request -> input('defaultimage'));

        }
        $suppliers = new Supplier;

        $suppliers -> name = $request -> name ;
        $suppliers -> image = $path;
        $suppliers -> address = $request -> address ;
        $suppliers -> postal_code = $request -> code ;
        $suppliers -> zip_code = $request -> code ;
        $suppliers -> region = $request -> region ;
        $suppliers -> city = $request -> city ;
        $suppliers -> country = $request -> country ;
        $suppliers -> contact_phone = $request -> c_phone ;
        $suppliers -> contact_email = $request -> email ;
        $suppliers -> contact_title = $request -> c_title ;
        $suppliers -> contact_name = $request -> c_name ;
        $suppliers -> contact_fax = $request -> fax ;

        $suppliers -> save();

        return redirect()->route('suplier.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Supplier::findOrFail($request -> id_supplier);

        $uploadedImage= $request->file('imageedit');

        if($uploadedImage !== NULL)
        {
            $old = str_replace('/storage', '', $request->image_old);
            $nama_file=Storage::url($old);
            if(File::exists('.'.$nama_file))
            {
                File::delete('.'.$nama_file);
            }
            $path   = $this->uploadImage($request ,$uploadedImage);
        }
        else{
            $path = str_replace('/storage/', 'public/', $request->image_old);

        }

        $update -> name = $request -> name ;
        $update -> image= $path;
        $update -> address = $request -> address ;
        $update -> postal_code = $request -> code ;
        $update -> zip_code = $request -> code ;
        $update -> region = $request -> region ;
        $update -> city = $request -> city ;
        $update -> country = $request -> country ;
        $update -> contact_phone = $request -> c_phone ;
        $update -> contact_email = $request -> email ;
        $update -> contact_title = $request -> c_title ;
        $update -> contact_name = $request -> c_name ;
        $update -> contact_fax = $request -> fax ;

        $update -> save();


        $id = $request -> name;

        Session::flash('u_success','Supplier ' . $id . ' Telah diperbarui.');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Supplier::find($id);
        $defaultimage = "".$data -> image;

        switch ($defaultimage) {
            case 'public/no-image.jpg':
            $data -> delete();
            break;
            
            default:
            Storage::delete($data->image);
            $data -> delete();
            break;
        }


        return redirect()->route('suplier.index');
    }

    public function deleteAll(Request $request)
    {
        $ids = $request->ids;
        Supplier::whereIn('id',explode(",",$ids))
        ->delete();
        return response()->json(['success'=>"Produk berhasil Dihapus."]);
    }

    function uploadImage($request ,$image){
        $ext = '.'.$image->getClientOriginalExtension();
        $nama_file = $request -> input('name').$ext;

        $img = Image::make($image->getRealPath());

        $img->stream();

        $uploaded = Storage::disk('local')->put('public/supplier'.'/'.$nama_file, $img, 'public');

        $path = '';
        if($uploaded){
            $path = 'public/supplier/'.$nama_file;
        }
        return $path;
    }
}
