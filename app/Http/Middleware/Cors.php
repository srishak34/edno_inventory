<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Cors
{
    
    public function handle(Request $req, Closure $next)
    {
         return $next($req)
         ->header('Access-Control-Allow-Origin', '*')
         ->header('Access-Control-Allow-Origin', 'GET, POST, PUT, DELETE, OPTIONS');
        

    }
}
