@extends('layouts.adminLayout.admin_master')
@section('content')
  <!-- MAIN CONTENT-->
  <div class="main-content">
      <div class="section__content section__content--p30">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-lg-12">
                      <div class="card">
                          <div class="card-header">
                              Pengaturan <strong>Admin</strong>
                          </div>
                          <div class="card-body card-block">
                              <form action="" method="post" class="form-horizontal">
                                  <div class="row form-group">
                                      <div class="col col-md-3">
                                          <label for="hf-email" class=" form-control-label">Kata Sandi Saat ini</label>
                                      </div>
                                      <div class="col-12 col-md-9">
                                          <input type="password" id="current_pwd" name="current_pwd" placeholder="Masukkan Kata Sandi Saat ini..." class="form-control">
                                          <span class="help-block">Silakan masukkan kata sandi Anda</span>
                                      </div>
                                  </div>
                                  <div class="row form-group">
                                      <div class="col col-md-3">
                                          <label for="hf-password" class=" form-control-label">Kata Sandi Baru</label>
                                      </div>
                                      <div class="col-12 col-md-9">
                                          <input type="password" id="new_pwd" name="new_pwd" placeholder="Masukkan Kata Sandi Baru..." class="form-control">
                                          <span class="help-block">Silakan masukkan kata sandi Anda</span>
                                      </div>
                                  </div>
                                  <div class="row form-group">
                                      <div class="col col-md-3">
                                          <label for="hf-password" class=" form-control-label">Konfirmasi Kata Sandi</label>
                                      </div>
                                      <div class="col-12 col-md-9">
                                          <input type="password" id="confirm_pwd" name="confirm_pwd" placeholder="Masukkan Kata Sandi Konfirmasi..." class="form-control">
                                          <span class="help-block">Silakan masukkan kata sandi Anda</span>
                                      </div>
                                  </div>
                              </form>
                          </div>
                          <div class="card-footer">
                              <button type="submit" class="btn btn-primary btn-sm">
                                  <i class="fa fa-dot-circle-o"></i> Selesai
                              </button>
                              <button type="reset" class="btn btn-danger btn-sm">
                                  <i class="fa fa-ban"></i> Atur Ulang
                              </button>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                      <div class="copyright">
                          <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection
