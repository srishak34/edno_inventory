
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark bg-info">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Your Company
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
        
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
        
                    </ul>
        
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            <li class=" dropdown" style="padding: 5px;">
                                <a id="navbarDropdown" class="btn btn-outline-info dropdown-toggle" style="width: 100px; height: 40px;" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
        
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
        
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            <li>
                                <img class="rounded-circle" style="margin-left: 3px;" width="50" height="50" src="{{ asset('image/hello.jpg') }} ">
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li>
                    <a href="/home">Dashboard</a>
                </li>
                <li>
                    <a href="/suplier">Supplier</a>
                </li>
                <li>
                    <a href="/tambahan">Item</a>
                </li>
                <li>
                    <a data-toggle="collapse" href="#collapseComponents2" data-parent="#exampleAccordion">Inventaris</a>
                    <ul class="sidenav-second-level collapse" id="collapseComponents2">
                      <li>
                        <a href="/barang/tambahbaru">Tambah Barang Masuk</a>
                      </li>
                      <li>
                        <a href="#">Tambah Barang Keluar</a>
                      </li>
                      <li>
                        <a href="/suplier/tambah">Tambah Suplier</a>
                      </li>
                    </ul>
                </li>
                <li>
                  <a href="#collapseComponents3" data-toggle="collapse" data-parent="#exampleAccordion"></a>
                </li>
                <li>
                    <a data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">Laporan</a>
                    <ul class="sidenav-second-level collapse" id="collapseComponents">
                    <li>
                      <a href="#">Data Peminjaman</a>
                    </li>
                    <li>
                      <a href="#">Data Suplier</a>
                    </li>
                  </ul>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle">Menu</a>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->