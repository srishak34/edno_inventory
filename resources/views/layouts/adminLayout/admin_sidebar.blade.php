<!-- MENU SIDEBAR-->
<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="{{ url('/admin/dashboard')}}">
            <img src="{{asset('images/icon/logosidebar.png')}}" style="width:200px;" alt="Cool Admin" />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <li>
                    <a href="/admin/dashboard"><i class="fas fa-tachometer-alt"></i>Beranda</a>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-copy"></i>Supplier</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="/admin/suplier">Supplier</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-desktop"></i>Barang</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="/admin/item">Barang1</a>
                        </li>
                    </ul>
                </li>
                {{-- <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-copy"></i>Halaman</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="login.html">Masuk</a>
                        </li>
                        <li>
                            <a href="register.html">Daftar</a>
                        </li>
                        <li>
                            <a href="forget-pass.html">Lupa Password</a>
                        </li>
                    </ul>
                </li> --}}
              {{--   <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-desktop"></i>Element UI</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="button.html">Tombol</a>
                        </li>
                        <li>
                            <a href="badge.html">Lencana</a>
                        </li>
                        <li>
                            <a href="tab.html">Tabs</a>
                        </li>
                        <li>
                            <a href="card.html">Kartu</a>
                        </li>
                        <li>
                            <a href="alert.html">Lansiran</a>
                        </li>
                        <li>
                            <a href="progress-bar.html">Progress Bar</a>
                        </li>
                        <li>
                            <a href="modal.html">Modal</a>
                        </li>
                        <li>
                            <a href="switch.html">Switch</a>
                        </li>
                        <li>
                            <a href="grid.html">Grid</a>
                        </li>
                        <li>
                            <a href="fontawesome.html">Fontawesome Icon</a>
                        </li>
                        <li>
                            <a href="typo.html">Tipografi</a>
                        </li>
                    </ul>
                </li> --}}
                {{-- <li>
                    <a href="chart.html">
                        <i class="fas fa-chart-bar"></i>Chart</a>
                </li> --}}
            </ul>
        </nav>
    </div>
</aside>
<!-- END MENU SIDEBAR-->
