<!-- HEADER MOBILE-->
<header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
        <div class="container-fluid">
            <div class="header-mobile-inner">
                <a class="logo" href="index.html">
                    <img src="{{asset('images/icon/logo.png')}}" alt="CoolAdmin" />
                </a>
                <button class="hamburger hamburger--slider" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-tachometer-alt"></i>Beranda</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="index.html">Beranda 1</a>
                        </li>
                        <li>
                            <a href="index2.html">Beranda 2</a>
                        </li>
                        <li>
                            <a href="index3.html">Beranda 3</a>
                        </li>
                        <li>
                            <a href="index4.html">Beranda 4</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="chart.html">
                        <i class="fas fa-chart-bar"></i>Chart</a>
                </li>
                <li>
                    <a href="table.html">
                        <i class="fas fa-table"></i>Tabel</a>
                </li>
                <li>
                    <a href="form.html">
                        <i class="far fa-check-square"></i>Formulir</a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-calendar-alt"></i>Kalender</a>
                </li>
                <li>
                    <a href="map.html">
                        <i class="fas fa-map-marker-alt"></i>Maps</a>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-copy"></i>Halaman</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="login.html">Masuk</a>
                        </li>
                        <li>
                            <a href="register.html">Daftar</a>
                        </li>
                        <li>
                            <a href="forget-pass.html">Lupa Password</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-desktop"></i>Elemen UI</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="button.html">Tombol</a>
                        </li>
                        <li>
                            <a href="badge.html">Lencana</a>
                        </li>
                        <li>
                            <a href="tab.html">Tab</a>
                        </li>
                        <li>
                            <a href="card.html">Kartu</a>
                        </li>
                        <li>
                            <a href="alert.html">Lansiran</a>
                        </li>
                        <li>
                            <a href="progress-bar.html">Progress Bar</a>
                        </li>
                        <li>
                            <a href="modal.html">Modal</a>
                        </li>
                        <li>
                            <a href="switch.html">Switch</a>
                        </li>
                        <li>
                            <a href="grid.html">Grid</a>
                        </li>
                        <li>
                            <a href="fontawesome.html">Fontawesome Logo</a>
                        </li>
                        <li>
                            <a href="typo.html">Tipografi</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- END HEADER MOBILE-->
