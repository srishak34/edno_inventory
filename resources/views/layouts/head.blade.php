<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" ng-app="main">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Ednovate</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/image/nashicon.ico') }}" />

    <!-- Scripts -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <script src="/js/app.js"></script>

    <!-- Custom styles for this template -->
    <link href="{{url('css/simple-sidebar.css')}}" rel="stylesheet">
    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    @stack('itemView')
    @stack('galleryHome')
    @stack('supplierView')
    @stack('supplierViewDetail')
    
  

    <style>
        body {
            padding:0 !important;
        }
        .button:focus { outline:0 !important; }
        .modal-open{
          padding:0 !important;
          overflow-y: auto;
         }
 
    </style>
</head>