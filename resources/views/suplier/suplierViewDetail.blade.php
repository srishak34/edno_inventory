@extends('layouts.app')
@push('supplierViewDetail')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
@endpush
@section('content')
<div class="container-fluid ">
	<div class="row">
		
		<div class="col-sm-12">
			<div class="container-fluid"><ul class="nav nav-tabs">
				<li class="nav-item">
					<a class="nav-link active" href="#info">Information</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#edit">Edit</a>
				</li>				
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div id="info" class="container-fluid tab-pane active"><br>
					<div class="row justify-content-center">
						<div class="col-sm-4 form-group">
							<label>Supplier</label>
							<input disabled="disabled" type="text" name="v_name" value="{{ $supplier -> name }}" class="form-control" >

							<label>Address</label>
							<input disabled="disabled" type="text" name="v_address" value="{{ $supplier -> address }}" class="form-control" >

							<label>Zip Code or Postal Code</label>
							<input disabled="disabled" type="text" name="v_code" value="{{ $supplier -> zip_code }}" class="form-control" >

							<label>Region</label>
							<input disabled="disabled" type="text" name="v_region" value="{{ $supplier -> region }}" class="form-control" >

							<label>City</label>
							<input disabled="disabled" type="text" name="v_city" value="{{ $supplier -> city }}" class="form-control" >

							<label>Country</label>
							<input disabled="disabled" type="text" name="v_country" value="{{ $supplier -> country }}" class="form-control" >
						</div>
						<div class="col-sm-4 form-group">

							<label>Phone Number</label>
							<input disabled="disabled" type="text" name="v_number" value="{{ $supplier ->  contact_phone}}" class="form-control" >

							<label>Email</label>
							<input disabled="disabled" type="email" name="v_email" value="{{ $supplier ->  contact_email}}" class="form-control" >

							<label>Contact title</label>
							<input disabled="disabled" type="text" name="v_c_title" value="{{ $supplier -> contact_title  }}" class="form-control" >

							<label>Contact Name</label>
							<input disabled="disabled" type="text" name="v_c_name" value="{{ $supplier ->  contact_name}}" class="form-control" >

							<label>Fax</label>
							<input disabled="disabled" type="text" name="v_fax" value="{{ $supplier -> contact_fax }}" class="form-control">
						</div>
						<div class="col-sm-3"><!--right col-->
							<div class="text-center">
								<img src="{{\Illuminate\Support\Facades\Storage::url($supplier->image)}}" class="img-thumbnail" alt="avatar">								
							</div>
						</div><!--/col 4-->
					</div>
				</div>
				<div id="edit" class="container-fluid tab-pane fade"><br>
					<form action="{{ route('suplier.update', 'update') }}" enctype="multipart/form-data" method="post">
						{{ method_field('patch') }}
						{{ csrf_field()}}
						<input type="hidden" name="id_supplier" value="{{ $supplier -> id }}">
						<div class="row justify-content-center">						
							<div class="col-sm-4 form-group">


								<label>Supplier</label>
								<input type="text" name="name" class="form-control" value="{{ $supplier -> name }}" placeholder="Enter Supplier">

								<label>Address</label>
								<input type="text" name="address" class="form-control" value="{{ $supplier -> address }}" placeholder="Enter Address">

								<label>Zip Code or Postal Code</label>
								<input type="text" name="code" class="form-control" pattern="[\d+]{5,}" title="Please type your Code" maxlength="8" value="{{ $supplier -> zip_code }}" placeholder="Enter Code">

								<label>Region</label>
								<input type="text" name="region" class="form-control" value="{{ $supplier -> region }}" placeholder="Enter Region">

								<label>City</label>
								<input type="text" name="city" class="form-control" value="{{ $supplier -> city }}" placeholder="Enter City">

								<label>Country</label>
								<input type="text" name="country" class="form-control" value="{{ $supplier -> country }}" placeholder="Enter Country">

							</div>
							<div class="col-sm-4 form-group">


								<label>Phone Number</label>
								<input type="text" name="number" class="form-control" pattern="[\d+]{12,}" maxlength="13" value="{{ $supplier -> contact_phone }}" placeholder="Enter The Number">

								<label>Email</label>
								<input type="email" name="email" class="form-control" value="{{ $supplier -> contact_email }}" placeholder="Enter Email">

								<label>Contact title</label>
								<input type="text" name="c_title" class="form-control" value="{{ $supplier -> contact_title }}" placeholder="Enter Contact Title">

								<label>Contact Name</label>
								<input type="text" name="c_name" class="form-control" value="{{ $supplier -> contact_name }}" placeholder="Enter Contact Name">

								<label>Fax</label>
								<input type="text" name="fax" class="form-control" pattern="[\d+]{5,}" maxlength="8" value="{{ $supplier -> contact_fax }}" placeholder="Enter Fax">

								

								<button name="clear" style="display: inline-block; margin-top: 31px;" onclick="clearForm(this.form)" class="btn btn-outline-warning" type="button"><i class="fa fa-refresh"></i> Reset</button>

								<button style="display: inline-block; margin-top: 31px;" class="btn btn-outline-primary" type="submit"><i class="fa fa-save"></i> Save</button>

								
							</div>
							<div class="col-sm-3"><!--right col-->
								<div class="text-center">
									<img id="preview" src="{{\Illuminate\Support\Facades\Storage::url($supplier->image)}}" class="avatar img-thumbnail" style="width: 100%; height: 100%;" alt="avatar">									
									<input type="file" style="display: none;" accept="image/*" id="image" name="image" class="text-center center-block">
									<input type="hidden" name="image_old" id="image_old">
									<a href="javascript:changeProfile();">Change</a> |
									<a style="color: red" href="javascript:removeImage()">Remove</a>
									<input type="hidden" style="display: none" value="0" name="remove" id="remove">
								</div>
							</div><!--/col 4-->
						</div>
					</form>
					<form action="/supplierGallery/destroy/{{ $supplier -> id }}" method="post">
						{{ csrf_field() }}
						<input type="hidden" name="_method" value="DELETE">
						<button type="submit" class="btn btn-outline-danger" style="position: absolute; left: 695px; bottom: 17px;"><i class="fa fa-trash"></i> Delete</button>
					</form>
				</div>


			</div></div>
		</div><!--/col-9-->

	</div><!--/row-->
</div>
<script>
	function clearForm(oForm) {

		var elements = oForm.elements; 

		oForm.reset();

		for(i=0; i<elements.length; i++) {

			field_type = elements[i].type.toLowerCase();

			switch(field_type) {

				case "text": 
				case "email":
				elements[i].value = ""; 
				break;

				default: 
				break;
			}
		}
	}

	function changeProfile() {
		$('#image').click();
	}
	$('#image').change(function () {
		var imgPath = $(this)[0].value;
		var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
		if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg") {
			readURL(this);
		}
		else {
			alert("Please select image file (jpg, jpeg, png).");
			$('#image').val("");
		}
			
	});
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.readAsDataURL(input.files[0]);
			reader.onload = function (e) {
				$('#preview').attr('src', e.target.result);
				$('#remove').val(0);
			}
		}
	}
	function removeImage() {
		$('#preview').attr('src', '{{\Illuminate\Support\Facades\Storage::url($supplier->image)}}');
		$('#remove').val(1);
	}

	$(document).ready(function() {

		$(".nav-tabs a").click(function(){
			$(this).tab('show');
		});
	});
</script>   
@endsection
