@extends('layouts.adminLayout.admin_master')
@section('content')
<div class="main-content">
	<div class="animsition">
		<div class="container-fluid">
			<form action="{{ route('suplier.store') }}" enctype="multipart/form-data" method="POST">
				{{ csrf_field() }}
				<div class="row main-form">
					<div class="col-md-12 form">
						<div class="card">
							<div class="card-header">
								<strong>Daftar</strong>
								<small>Supplier</small>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="card-body card-block">
										<div class="form-group">
											<input type="hidden" name="id_supplier" id="supplier_id" value="">
											<label>Nama Supplier</label>
											<input type="text" name="name" id="u_name" class="form-control" placeholder="masukan nama Supplier" required="required">
										</div>
										<div class="form-group">
											<label>Gambar</label>
											<input type="file" required="required" id="imageedit" name="imageedit" accept="image/*" class="form-control" placeholder="masukan gambar">
											<input type="hidden" name="image_old" id="image_old" required="required">
											
										</div>
										<div class="form-group">
											<label>Alamat</label>
											<input type="text" name="address" id="u_address" class="form-control" placeholder="masukan alamat" required="required">
										</div>
										<div class="form-group">
											<label>Zip Code or Postal Code</label>
											<input type="text" name="code" id="u_code" class="form-control" pattern="[\d+]{5,}" maxlength="8" title="Please type your Code" placeholder="masukan Code" required="required">
											
										</div>
										<div class="form-group">
											<div class="rs-select2--light rs-select2--md">
												<select class="js-select2" name="property">
													<option selected="selected">All Properties</option>
													<option value="">Option 1</option>
													<option value="">Option 2</option>
												</select>
												<div class="dropDownSelect2"></div>
											</div>
										</div>
										<div class="form-group">
											<label>City</label>
											<input type="text" name="city" id="u_city" class="form-control" placeholder="dimana anda tinggal">

										</div>

									</div>
								</div>
								<div class="col-md-6">
									<div class="card-body card-block">
										<div class="form-group">
											<label>Nasional</label>
											<input type="text" name="country" id="u_country" class="form-control" placeholder="asal negara">
										</div>
										<div class="form-group">
											<label>Nomor Telepon</label>
											<input type="text" name="number" id="u_number" class="form-control" pattern="[\d+]{12,}" title="Please Input Your Phone Number" maxlength="13" placeholder="masukan nomer telepon anda">   

										</div>
										<div class="form-group">
											<label>Email</label>
											<input type="email" name="email" id="u_email" class="form-control" placeholder="masukan email anda">
										</div>
										<div class="form-group">

											<label>Judul Kontak</label>
											<input type="text" name="c_title" id="u_c_title" class="form-control" placeholder="judul kontak anda">
										</div>
										<div class="form-group">
											<label>Nama Kontak</label>
											<input type="text" name="c_name" id="u_c_name" class="form-control" placeholder="nama kontak anda">
										</div>
										<div class="form-group">

											<label>Fax</label>
											<input type="text" name="fax" id="u_fax" class="form-control" pattern="[\d+]{5,}" maxlength="8" placeholder="masukan Fax">
										</div>
									</div>	
								</div>
							</div>
							<div class="row">
								<div class="card-body">
									<div class="col-md-4 offset-4" style="padding-bottom: 5px;" >
										<input type="reset" name="" class="btn btn-outline-danger" style="width:250px;">                               
										<input type="submit" name="" class="btn btn-outline-primary mt-1" value="Update" style="width:250px;">
									</div>
								</div>
							</div>                                 
							<div class="col-md-4"></div>
						</div>{{-- card --}}
					</div>{{-- col-md-12 form --}}
				</div>{{-- row main-form --}}
			</form>
			<div class="row">
				<div class="col-md-12">
					<div class="copyright">
						<p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
					</div>
				</div>
			</div>
		</div>{{-- container-fluid --}}
	</div>{{-- akhir animstion --}}
</div>{{-- main content --}}
@endsection