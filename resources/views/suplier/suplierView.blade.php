@extends('layouts.adminLayout.admin_master')

@section('content')
<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="animsition">        
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <!-- DATA TABLE -->
                        <h3 class="title-5 m-b-35">Data Supplier</h3>
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                                <div class="rs-select2--light rs-select2--md">
                                    <select class="js-select2" name="property">
                                        <option selected="selected">Semua Properti</option>
                                        <option value="">Pengaturan 1</option>
                                        <option value="">Pengaturan 2</option>
                                    </select>
                                    <div class="dropDownSelect2"></div>
                                </div>
                                <div class="rs-select2--light rs-select2--md text-center">
                                    <select class="js-select2" name="time">
                                        <option selected="selected">Hari ini</option>
                                        <option value="">3 Hari lalu</option>
                                        <option value="">1 Minggu lalu</option>
                                    </select>
                                    <div class="dropDownSelect2"></div>
                                </div>
                                <button class="au-btn-filter">
                                    <i class="zmdi zmdi-filter-list"></i>filter</button>
                                </div>
                                <div class="table-data__tool-right">
                                    <button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#add">
                                        <i class="zmdi zmdi-plus"></i>Tambahkan Supplier</button>
                                        <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                            <select class="js-select2" name="type">
                                                <option selected="selected">Export</option>
                                                <option value="">Pengaturan 1</option>
                                                <option value="">Pengaturan 2</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2" >
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </th>
                                                <th>Nama</th>
                                                <th>Gambar</th>
                                                <th>Negara</th>
                                                <th>No.Telp</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($supplier as $key => $data)
                                            <tr class="tr-shadow">
                                                <td>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </td>
                                                <td>{{ $data -> name }}</td>
                                                <td style="width: 160px; height: 100px;"><img src="{{Storage::url($data->image)}}" class="img-responsive" "></td>

                                                <td>
                                                    <span class="status--process">{{ $data -> country }}</span>
                                                </td>
                                                <td>{{ $data -> contact_phone }}</td>
                                                <td>
                                                    <div class="table-data-feature">
                                                       {{--  <button class="item" data-toggle="tooltip" data-placement="top" title="Send">
                                                            <i class="zmdi zmdi-mail-send"></i>
                                                        </button> --}}
                                                        <button type="button" class="item" data-name="{{ $data -> name }}"  data-address="{{ $data -> address }}" data-zip="{{ $data -> zip_code }}" data-region="{{ $data -> region }}" data-city="{{ $data -> city }}" data-country="{{ $data -> country }}" data-phone="{{ $data -> contact_phone }}" data-email="{{ $data -> contact_email }}" data-ctitle="{{ $data -> contact_title }}" data-cname="{{ $data -> contact_name }}" data-fax="{{ $data -> contact_fax }}" data-image="{{ Storage::url($data->image) }}" data-id="{{ $data -> id }}" data-toggle="modal" data-target="#edit"><i class="zmdi zmdi-edit"></i></button>
                                                        <form id="FormDeleteTime" action="{{ route('suplier.destroy', ['id' => $data -> id]) }}" method="post">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="_method" value="DELETE">

                                                            <button type="submit" class="item" onclick="alertdelete()" data-toggle="tooltip" data-placement="top" title="Delete"><i class="zmdi zmdi-delete"></i></button>
                                                        </form>                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {{ $supplier->links() }}
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>                
            </div>
        </div>
        <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Supplier</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <div class="container-fluid ">
                <form action="{{ route('suplier.store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row form-group">
                        <div class="col-sm-4">
                            <label class="form-control-label">Nama</label>
                            <input type="text" name="name" placeholder="Nama Supplier" class="form-control">                    

                            <label class="form-control-label">Alamat</label>
                            <textarea name="address" class="form-control" placeholder="Masukkan Alamat Rumah" rows="4" style="resize: none;" maxlength="40"></textarea>

                            <label class="form-control-label">Kode Pos</label>
                            <input type="text" name="code" onkeypress="return isNumberKey(event)" required="required" class="form-control" title="Mohon Masukkan Kode Pos Anda" maxlength="5" placeholder="Masukkan Kode Pos">

                            <label class="form-control-label">Provinsi</label>
                            <input type="text" name="region" placeholder="Provinsi Asal" class="form-control">

                            <label class="form-control-label">Kota</label>
                            <input type="text" name="city" placeholder="Kota Asal" class="form-control">



                        </div>
                        <div class="col-sm-4">
                            <label class="form-control-label">Negara</label>
                            <input type="text" name="country" placeholder="Negara Asal" class="form-control">

                            <label class="form-control-label">Judul Kontak</label>
                            <input type="text" name="c_title" placeholder="Judul Kontak" class="form-control">

                            <label class="form-control-label">Nama Kontak</label>
                            <input type="text" name="c_name" placeholder="Nama Kontak" class="form-control">

                            <label class="form-control-label">Kontak Telepon</label>
                            <input type="text" onkeypress="return isNumberKey(event)" name="c_phone" maxlength="12" placeholder="Nomor Telepon" class="form-control">

                            <label class="form-control-label">Fax</label>
                            <input type="text" onkeypress="return isNumberKey(event)" maxlength="6" name="fax" placeholder="Nomor Fax" class="form-control">

                            <label class="form-control-label">Email</label>
                            <input type="email" name="email" placeholder="EmailAnda@domain.com" class="form-control">
                        </div>
                        <div class="col-sm-4 text-center">
                            <label class="form-control-label">Gambar</label>
                            <div class="text-center">
                                <img id="preview" src="{{ Storage::url('no-image.jpg') }}" class="img-thumbnail" style="width: 100%; height: 100%;" alt="avatar">
                                <div style="width: 100%;"></div>                                    
                                <input type="file" accept="image/*" style="display: none;" id="image" name="image" class="text-center center-block">
                                <input id="defaultimage" type="hidden" value="{{ Storage::url('no-image.jpg') }}" name="defaultimage">                                    
                                <button type="button" style="color: blue;" onclick="inputImage()">Tambah</button>
                                <button id="removeAdd" class="remove-hidden" type="button" onclick="removeImage()" style="color: red;">| Hapus</button>
                                <input type="hidden" style="display: none" value="0" name="remove" id="remove">
                            </div>

                        </div>
                    </div>
                    

                </div>

                <div class="modal-footer">
                    <button type="button" onclick="resetForm()" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="reset" id="resetInput" class="btn btn-outline-warning">Kembali ke Awal</button>
                    {{-- <input id="resetInput" type="reset"  value="Kembali ke Awal"> --}}

                    <input type="submit" class="btn btn-primary" value="Tambahkan Supplier">
                </form>

            </div>

            

        </div>
    </div>
</div>
</div>


<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ubah Supplier</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <div class="container-fluid ">
     <form action="{{ route('suplier.update', 'update') }}" enctype="multipart/form-data" method="post">
        {{ method_field('patch') }}
        {{ csrf_field()}}
        <input type="hidden" name="id_supplier" id="supplier_id" value="">
        <div class="row form-group">
            <div class="col-sm-4">
                <label class="form-control-label">Nama</label>
                <input type="text" name="name" id="u_name" placeholder="Nama Supplier" class="form-control">                    

                <label class="form-control-label">Alamat</label>
                <textarea name="address" id="u_address" class="form-control" rows="4" style="resize: none;" maxlength="40"></textarea>

                <label class="form-control-label">Kode Pos</label>
                <input type="text" name="code" id="u_code" onkeypress="return isNumberKey(event)" required="required" class="form-control" title="Mohon Masukkan Kode Pos Anda" maxlength="5" placeholder="Masukkan Kode Pos">

                <label class="form-control-label">Provinsi</label>
                <input type="text" name="region" id="u_region" placeholder="Provinsi Asal" class="form-control">

                <label class="form-control-label">Kota</label>
                <input type="text" name="city" id="u_city" placeholder="Kota Asal" class="form-control">



            </div>
            <div class="col-sm-4">
                <label class="form-control-label">Negara</label>
                <input type="text" name="country" id="u_country" placeholder="Negara Asal" class="form-control">

                <label class="form-control-label">Judul Kontak</label>
                <input type="text" name="c_title" id="u_c_title" placeholder="Judul Kontak" class="form-control">

                <label class="form-control-label">Nama Kontak</label>
                <input type="text" name="c_name" id="u_c_name" placeholder="Nama Kontak" class="form-control">

                <label class="form-control-label">Kontak Telepon</label>
                <input type="text" name="c_phone" id="u_c_phone" onkeypress="return isNumberKey(event)" maxlength="12" placeholder="Nomor Telepon" class="form-control">

                <label class="form-control-label">Fax</label>
                <input type="text" onkeypress="return isNumberKey(event)" maxlength="6" name="fax" id="u_fax" placeholder="Nomor Fax" class="form-control">

                <label class="form-control-label">Email</label>
                <input type="email" name="email" id="u_email" placeholder="Emailanda@domain.com" class="form-control">
            </div>
            <div class="col-sm-4 text-center">
                <label class="form-control-label">Gambar</label>
                <div class="text-center">
                    <img id="u_image" src="" class="img-thumbnail" style="width: 100%; height: 100%;" alt="avatar">
                    <div style="width: 100%;"></div>                                   
                    <input type="file" style="display: none;" accept="image/*" id="imageedit" name="imageedit"><br>
                    <input type="hidden" name="image_old" id="image_old">                            
                    <button type="button" style="color: blue;" onclick="editImage()">Ubah</button>
                    <button id="removeEdit" class="remove-hidden" type="button" onclick="removeImageEdit()" style="color: red;">| Semula</button>
                    <input type="hidden" style="display: none" value="0" name="remove" id="remove">
                </div>

            </div>
        </div>
        

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <input type="reset" class="btn btn-outline-warning" value="Reset">

        <input type="submit" class="btn btn-primary" value="Save Changes">

    </div>
</form>


</div>
</div>
</div>
</div>

<script type="text/javascript">
 // user MEmbatalkan Add Supplier
function resetForm() {
    $('#resetInput').click();
}

// Create Image JS
function inputImage() {
    $('#image').click();
}
$('#image').change(function () {
    var imgPath = $(this)[0].value;
    var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg") {
        var oFile = document.getElementById("image").files[0];
            if (oFile.size > 1000000) { // 1 mb for bytes.            
                alert("Ukuran file harus di bawah 1 MB!");
                $('#image').val("");                          
            }
            readInputURL(this);
        }
        else {
            alert("Please select image file (jpg, jpeg, png).");
            $('#image').val("");
        }
        
    });
function readInputURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[0]);
        reader.onload = function (e) {
            $('#preview').attr('src', e.target.result);
            $('#remove').val(0);
            $('#removeAdd').removeClass('remove-hidden');
            $('#removeAdd').addClass('remove-show');
        }
    }
}
function removeImage() {
    $('#preview').attr('src', $('#defaultimage').val());
    $('#remove').val(1);
    $('#image').val("");
    $('#removeAdd').removeClass('remove-show');
    $('#removeAdd').addClass('remove-hidden');
}

// Input Only Number
function isNumberKey(evt) {
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

return true;
}

// Delete Notif
$("#FormDeleteTime").submit(function (event) {
 var x = confirm("yakin ingin menghapusnya?");
 if (x) {
    return true;
}
else {

    event.preventDefault();
    return false;
}

});

// Edit Image JS
function editImage() {        
    $('#imageedit').click();
}
$('#imageedit').change(function () {
    var imgPath = $(this)[0].value;
    var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg") {
        var oFile = document.getElementById("imageedit").files[0];
             if (oFile.size > 1000000) { // 2 mb for bytes.            
                alert("Ukuran file harus di bawah 1 MB!");
                $('#imageedit').val("");                          
            }
            readEditURL(this);;
        }
        else {
            alert("Silakan pilih file gambar (jpg, jpeg, png).");
            $('#imageedit').val("");
           
        }

    });

function readEditURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[0]);
        reader.onload = function (e) {
            $('#u_image').attr('src', e.target.result);
            $('#remove').val(0);
            $('#removeEdit').removeClass('remove-hidden');
            $('#removeEdit').addClass('remove-show');
        }
    }
}
function removeImageEdit() {        
    $('#u_image').attr('src', '' + $('#image_old').val());
    $('#remove').val(1);
    $('#imageedit').val("");
    $('#removeEdit').removeClass('remove-show');
    $('#removeEdit').addClass('remove-hidden');
}


$('#edit').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var name = button.data('name')
    var image = button.data('image')
    var address = button.data('address')
    var zip = button.data('zip')
    var region = button.data('region')
    var city = button.data('city')
    var country = button.data('country')
    var phone = button.data('phone')
    var email = button.data('email')
    var ctitle = button.data('ctitle')
    var cname = button.data('cname')
    var fax = button.data('fax')
    var supplier = button.data('id')

    var modal = $(this)

    modal.find('.modal-body #u_name').val(name)
    modal.find('.modal-body #u_image').attr('src', '' + image)
    modal.find('.modal-body #image_old').val(image)    
    modal.find('.modal-body #u_address').val(address)
    modal.find('.modal-body #u_code').val(zip)
    modal.find('.modal-body #u_region').val(region)
    modal.find('.modal-body #u_city').val(city)
    modal.find('.modal-body #u_country').val(country)
    modal.find('.modal-body #u_c_phone').val(phone)
    modal.find('.modal-body #u_email').val(email)
    modal.find('.modal-body #u_c_title').val(ctitle)
    modal.find('.modal-body #u_c_name').val(cname)
    modal.find('.modal-body #u_fax').val(fax)
    modal.find('.modal-body #supplier_id').val(supplier)        
});




$(document).ready(function () {


    $('#master').on('click', function(e) {
       if($(this).is(':checked',true))
       {
        $(".sub_chk").prop('checked', true);
    } else {
        $(".sub_chk").prop('checked',false);
    }
});


    $('.delete_all').on('click', function(e) {


        var allVals = [];
        $(".sub_chk:checked").each(function() {
            allVals.push($(this).attr('data-id'));
        });


        if(allVals.length <= 0 )
        {
            alert("Please select row.");
        }  else {


            var check = confirm("Anda yakin ingin menghapus baris ini?");
            if(check == true){


                var join_selected_values = allVals.join(",");


                $.ajax({
                    url: $(this).data('url'),
                    type: 'DELETE',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: 'ids='+join_selected_values,
                    success: function (data) {
                        if (data['success']) {
                            $(".sub_chk:checked").each(function() {
                                $(this).parents("tr").remove();
                            });
                            alert(data['success']);
                        } else if (data['error']) {
                            alert(data['error']);
                        } else {
                            alert('Aduh, ada yang salah !!');
                        }
                    },
                    error: function (data) {
                        alert(data.responseText);
                    }
                });


                $.each(allVals, function( index, value ) {
                  $('table tr').filter("[data-row-id='" + value + "']").remove();
              });
            }
        }
    });



});
</script>
@endsection
