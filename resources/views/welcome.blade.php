<!doctype html >
<html lang="{{-- {{ app()->getLocale() }} --}}" ng-app="main">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>FocusStore</title>
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('/images/icon/logo.png') }}" />

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

  <!-- CSS Bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

  <!-- Font Awesome -->
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/hover.css">

  <!-- Styles -->
  <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
  <link rel="stylesheet" href="/css/hover.css">
  <link rel="stylesheet" type="text/css" href="/css/gallery.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.2/angular.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.2/angular-route.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.2/angular-resource.min.js"></script>
  <script src="/js/configuration.js"></script>
  <script src="/js/controllers.js"></script>
  <script src="/js/main.js"></script>

</head>

<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-right">
        @if (Route::has('login'))
        <div class="top-right links">
          @auth
          <a href="{{ url('/home') }}" class="{{-- btn btn-dark --}}" style="display: none;">Beranda</a>
          @else
          <a href="{{ url('/admin') }}"><button class="btn btn-outline-primary"><i class="fa fa-sign-in"></i>Masuk</button></a>
          <a href="{{ route('register') }}"><button class="btn btn-outline-primary"><i class="fa fa-sign"></i>Daftar</button></a>
          @endauth
        </div>
        @endif
      </div>
    </div>
  </div>{{-- header --}}
  <nav class="navbar navbar-expand-lg navbar-light  sticky-top" style="background-color: #f5f5f5;">
    <div class="container" style="height: 50px;">
      <div class="col-md-6">
        <a class="navbar-brand pull-left" href="#"><img src="{{asset('images/icon/logosidebar.png')}}" style="width:200px;" alt="Cool Admin" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="col-md-6">
        <div class="collapse navbar-collapse pull-right" id="navbarSupportedContent ">
          <ul class="nav navbar-nav navbar-right ">
            @if(Auth::user())
            <li class="nav-item active">
              <a class="nav-link" href="/admin/dashboard">Beranda <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="#">Produk</a>
            </li>
            <li class="nav-item dropdown active">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Kategori
              </a>
            </li>
            @endif            
          </ul>
        </div>
      </div>
    </div>
  </nav>{{-- navbar --}}

  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" id="corousel-iner">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="{{ asset('img/img13.jpg')}}" alt="First slide">
        <div class="carousel-caption d-none d-md-block">
          <p style="color: deeppink; font-size: 50px; font-weight: bold;">Semua Tentang Diriku</p>
          <button class="btn btn-danger btn-lg">LEBIH BANYAK</button>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="{{ asset('img/img11.jpg')}}" alt="Second slide">
        <div class="carousel-caption d-none d-md-block">
          <p style="color: deeppink; font-size: 50px; font-weight: bold;">Semua Tentang Diriku</p>
          <button class="btn btn-outline-secondary" style="width: 40px;"><i class="fa fa-mail-forward"></i></button>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="{{ asset('img/img5.jpg')}}" alt="Third slide">
        <div class="carousel-caption d-none d-md-block">
          <p style="color: deeppink; font-size: 50px; font-weight: bold;">Semua Tentang Diriku</p>
          <button class="btn btn-danger btn-lg">LEBIH BANYAK</button>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <div class="container" >
    <div class="row text-center mt-5">
      <div class="col-md-12">
        <h1 style="color: dodgerblue;">Item Terlaris</h1>
      </div>
    </div>

    <div class="row mt-5" ng-controller="itemController">
      <div class="col-md-4 mt-1" ng-repeat="(key, data) in dataItem">
        <div class="card no-border">
          <div class="img-fluid" style="height: 200px;">
            <img src="@{{ data.image }}" class="img-responsive" alt="img" style="height: 100%; width:100%;" >
          </div>
          <div class="card-body" style="background-color: ">
            <h5 class="card-title text-center">informasi</h5>
            <p class="card-text">
              <table>
                <tr>
                  <td>Nama item</td>
                  <td>:</td>
                  <td>@{{ data.nama }}</td>
                </tr>
                <tr>
                  <td>Deskripsi</td>
                  <td>:</td>
                  <td>@{{ data.deskripsi }}</td>
                </tr>
                <tr>
                  <td>Stok</td>
                  <td>:</td>
                  <td>@{{ data.qty }}</td>
                </tr>
              </table>
            </p>
            <a href="#"><button class="btn btn-outline-primary"><i class="fa fa-shopping-cart"> </i></button></a>
            <a href="#"><button class="btn btn-outline-primary" data-toggle="modal data-target="#tamabahData"><i class="fa fa-comments-o"> </i></button></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid help mt-5 pt-5">
    <div class="container">
      <div class="row">
        <div class="col-md-6 offset-md-6">
          <div class="row">
            <h1>Ingin menggunakan layanan kami?</h1>
          </div>
          <div class="row mt-3">
            <h5>um is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw.um is slechts een proeftekst uit het drukkerij- en zetterijwezen. Lorem Ipsum is de standaard proeftekst in deze bedrijfstak sinds de 16e eeuw.</h5>
          </div>

          <div class="row mt-3 pb-4">
            <div class="col-md-6">
              <ul style="list-style-type: none; margin: 0px; padding: 0px;">
                <li><i class="fa fa-check"></i>Present vesnaonwe</li>
                <li><i class="fa fa-check"></i>Present vesnaonwe</li>
                <li><i class="fa fa-check"></i>Present vesnaonwe</li>
                <li><i class="fa fa-check"></i>Present vesnaonwe</li>
              </ul>
            </div>

            <div class="col-md-6">
              <ul style="list-style-type: none; margin: 0px; padding: 0px;">
                <li><i class="fa fa-check"></i>Present vesnaonwe</li>
                <li><i class="fa fa-check"></i>Present vesnaonwe</li>
                <li><i class="fa fa-check"></i>Present vesnaonwe</li>
                <li><i class="fa fa-check"></i>Present vesnaonwe</li>
              </ul>
            </div>


          </div>
        </div>
      </div>
    </div>
  </div>

  <footer class="pt-5 pb-4">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="media">
            <div class="footer-media mr-3"><i class="fa fa-maps-maker"></i></div>
            <div class="media-body">
              <h5 class="mt-0">Alamat</h5> 0909 Marmora Jalan,Palembnag<br> D02DD_-
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="media">
            <div class="footer-media mr-3"><i class="fa fa-phone"></i></div>
            <div class="media-body">
              <h5 class="mt-0">No.Telp</h5> 0808080880
              <br> 08080808++
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="media">
            <div class="footer-media mr-3"><i class="fa fa-facebook-f"></i></div>
            <div class="media-body">
              <h5 class="mt-0">Ikuti Kami</h5> ADMIN.com/Facebookn
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>



  <!-- JS Bootstrap -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>

</body>

</html>
