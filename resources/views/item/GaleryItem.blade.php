@extends('layouts.adminLayout.admin_master')
<link rel="stylesheet" href="/css/hover.css">
@push('galleryHome')
<link rel="stylesheet" type="text/css" href="/css/gallery.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.2/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.2/angular-route.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.2/angular-resource.min.js"></script>
<script src="/js/configuration.js"></script>
<script src="/js/controllers.js"></script>
<script src="/js/main.js"></script>
@endpush
@section('content')
<div class="main-content" ng-app="main">
    <div class="animstion">
        <div class="container-fluid" >
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="title-5 m-b-35">Galery Item</h3>
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <div class="rs-select2--light rs-select2--md">
                                <select class="js-select2" name="property">
                                    <option selected="selected">All Properties</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>{{-- rs-select2-light rs-select2-md --}}
                            <div class="rs-select2--light rs-select2--sm">
                                <select class="js-select2" name="time">
                                    <option selected="selected">Today</option>
                                    <option value="">3 Days</option>
                                    <option value="">1 Week</option>
                                </select>
                                <div class="dropDownSelect2"></div>                               
                            </div>{{-- today --}}
                            <button class="au-btn-filter"><i class="zmdi zmdi-filter-list"></i>filters</button>
                        </div>{{-- table-data__tool-left --}}
                        <div class="table-data__tool-right">
                            <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#tamabahData"><i class="zmdi zmdi-plus"></i>add item</button>
                            <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                <select class="js-select2" name="type">
                                    <option selected="selected">Export</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                </select>
                                <div class="dropDownSelect2"></div>                           
                            </div>                        
                        </div>{{-- table-data__tool-right --}}
                    </div>{{-- table-data_tool --}}
                    <div class="table-data-content" ng-controller="itemController">
                        <div class="section__content section__content--p30">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-4 mt-1 ml-0" ng-repeat="(key, data) in dataItem">
                                       <div class="profile">
                                           <img src="@{{ data.image }}" class="img-fluid" style="width: 100%; height: 100%;" alt="photos">
                                           <div class="details">
                                               <ul style="margin-top: 8rem;">
                                                 <li><a href="/itemGalery/@{{ data.id }}"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                 <li><a href="/itemGalery/@{{ data.id }}"><i class="fa fa-eye" aria-hidden="true"></i></a></li>
                                                 <li><a href="#"><i class="fa fa-edit" aria-hidden="true"></i></a></li>
                                                 <li>
                                                    <button style="display: none;" type="submit" name="delete" id="delete" ng-click="deleteThis(key)" style="font-size: 10px;">Delete</button>
                                                    <a href="javascript:deleteProfile();"><i class="fa fa-trash" aria-hidden="true"></i></a>                           
                                                </li> 
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>{{-- row pertama --}}
        <div class="row">
         <div class="col-md-12">
            <div class="copyright">
                <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
            </div>
        </div>

    </div>
</div>
</div>
<script>
    function deleteProfile() {
        $('#delete').click();
    }
</script>
@endsection