@extends('layouts.adminLayout.admin_master')


@section('content')
<div class="main-content">
    <div class="animsition">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title-5 m-b-35">Data Barang</h3>
                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                            <div class="rs-select2--light rs-select2--md">
                                <select class="js-select2" name="property">
                                    <option selected="selected">Semua Properti</option>
                                    <option value="">Pengaturan 1</option>
                                    <option value="">Pengaturan 2</option>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>{{-- rs-select2-light rs-select2-md --}}
                            <div class="rs-select2--light rs-select2--md text-center" >
                                <select class="js-select2" name="time">
                                    <option selected="selected">Hari Ini</option>
                                    <option value="">3 Hari lalu</option>
                                    <option value="">1 Minggu lalu</option>
                                </select>
                                <div class="dropDownSelect2"></div>                               
                            </div>{{-- today --}}
                            <button class="au-btn-filter"><i class="zmdi zmdi-filter-list"></i>Filter</button>
                        </div>{{-- table-data__tool-left --}}
                        <div class="table-data__tool-right">
                            <button type="button" class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#tambahData"><i class="zmdi zmdi-plus"></i>Tambah Item</button>
                            <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                <select class="js-select2" name="type">
                                    <option selected="selected">Export</option>
                                    <option value="">Penguturan 1</option>
                                    <option value="">Pengaturan 2</option>
                                </select>
                                <div class="dropDownSelect2"></div>                           
                            </div>                        
                        </div>{{-- table-data__tool-right --}}
                    </div>{{-- table-data_tool --}}
                    <div class="table-responsive table-responsive-data2">

                        <table class="table table-data2">
                            <thead>
                                <tr>
                                    <th>
                                        <label class="au-checkbox">
                                            <input type="checkbox">
                                            <span class="au-checkmark"></span>
                                        </label>
                                    </th>
                                    <th>Nama Barang</th>
                                    <th>Gambar</th>
                                    <th>Deskripsi</th>
                                    <th>kuantitas</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="mt-2">
                                @foreach($items as $key => $data)
                                <tr class="tr-shadow mt-3">
                                    <td><input type="checkbox" class="sub_chk" data-id="{{$data-> id}}"></td>
                                    <td>{{ $data -> nama }}</td>
                                    <td style="width: 160px; height: 100px;"><img src="{{Storage::url($data->image)}}" class="img-responsive" "></td>
                                    <td>{{ $data -> deskripsi }}</td>
                                    <td>{{ $data -> qty }}</td>
                                    <td>
                                        <div class="table-data-feature">
                                            {{-- <button class="item" data-toggle="tooltip" data-placement="top" title="Send">
                                                <i class="zmdi zmdi-mail-send"></i>
                                            </button> --}}
                                            <button class="item" title="Edit" data-nama="{{  $data -> nama }}" data-deskripsi="{{ $data -> deskripsi }}" data-code="{{ $data -> code }}" data-qty="{{ $data -> qty}}" data-image="{{ Storage::url($data->image) }}" data-id="{{ $data -> id }}" data-toggle="modal" data-target="#edit" >
                                                <i class="zmdi zmdi-edit"></i>
                                            </button>
                                            
                                            <form id="FormDeleteTime" action="{{ route('item.destroy', ['id' => $data -> id]) }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="item" data-toggle="tooltip" data-placement="top" onclick="alertdelete()" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </form>

                                        </div>{{-- akhir div aksi --}}
                                    </td>                                
                                </tr>
                                @endforeach                                
                            </tbody>
                        </table>
                    </div>{{-- tabel data item --}}

                </div>{{-- colp pertama --}} 


            </div>{{-- row --}}
            <div class="row align-self-center">
                <div class="col-sm-12"> 
                    {{ $items->links() }}
                </div>
            </div>
            
            
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright">
                        <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>

        </div>{{-- container-fluid --}}   
    </div>{{-- animstion --}}    
</div>{{-- main-content --}}

<div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Tambah Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>                                            
            </div>{{-- modal header --}}
            <div class="modal-body">
                <form action="{{ route('item.store') }}" enctype="multipart/form-data" method="POST">
                    {{ csrf_field() }}
                    <div class="row form-group">
                        <div class="col-sm-6 ">
                            <div class="card-body card-block">
                                <label>Nama Barang</label>
                                <input type="text" name="nama"  class="form-control" placeholder="Masukkan Nama Barang" required="required">
                                
                                <label>Deskripsi</label>
                                <textarea rows="4" style="resize: none;" type="text" name="deskripsi" class="form-control" placeholder="Tulis Deskripsi Barang" required="required"></textarea>
                                
                                <label>Kode</label>
                                <input type="text" onkeypress="return isNumberKey(event)" name="code"  class="form-control" placeholder="Masukkan Kode Barang" required="required">
                                
                                <label>Kuantitas</label>
                                <input type="number" onkeypress="return isNumberKey(event)" name="qty" class="form-control" placeholder="Masukkan Kuantitas Barang" required="required">

                            </div>                                                 
                        </div>{{-- akhir col-sm-6 form --}}
                        <div class="col-sm-6 text-center">
                            <label class="form-control-label">Gambar</label>
                            <div class="text-center">

                                <img id="preview" src="{{ Storage::url('no-image.jpg') }}" class="" style="width: 100%; height: 100%;" alt="avatar">                                                                   
                                <input type="file" accept="image/*" id="image" name="image" class="text-center center-block m-1" style="display: none;">
                                <div style="width: 100%;"></div>
                                <input type="hidden" value="{{ Storage::url('no-image.jpg') }}" name="defaultimage">                                    
                                <button type="button" style="color: blue;" onclick="inputImage()">Tambah</button> 
                                <button id="removeAdd" type="button" class="remove-hidden" onclick="removeImage()" style="color: red;">| Hapus</button>
                                <input type="hidden" style="display: none" value="0" name="remove" id="remove">
                            </div>
                        </div> {{-- gambar--}}
                        <div class="modal-footer col-md-12 pull-right">
                            <button type="button" onclick="resetForm()" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
                            <input id="resetInput" type="reset" name="reset" class="btn btn-outline-warning" value="Reset">
                            <button type="submit" class="btn btn-outline-primary">Tambahkan Barang</button>
                        </div>
                    </div>{{-- akhir row dalam form   --}}                                              
                </form>{{-- akhir form create --}}
            </div>{{-- modal-body--}}
        </div>{{-- modal content --}}                                    
    </div>{{-- modal-dialog modal-lg --}} 
</div><!-- modal tambah data item -->
{{-- modal edit --}}
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Ubah Data Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('item.update','update') }}" enctype="multipart/form-data" method="POST">
                    {{ method_field('patch') }}
                    {{ csrf_field() }}
                    <input type="hidden" name="id_item" id="item_id" value="">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label>Nama Barang</label>
                                    <input type="text" name="nama" id="u_nama" class="form-control" placeholder="Masukkan Nama Barang" required="required">
                                </div>
                                <div class="form-group">
                                    <label>Deskripsi</label>
                                    <textarea rows="4" style="resize: none;" type="text" name="deskripsi" id="u_deskripsi" class="form-control" placeholder="Tulis Deskripsi Barang" required="required"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Kode</label>
                                    <input type="text" onkeypress="return isNumberKey(event)" name="code" id="u_code" class="form-control" placeholder="Masukkan Kode Barang" required="required">
                                </div>
                                <div class="form-group">
                                    <label>Kuantitas</label>
                                    <input type="number" name="qty" id="u_qty" class="form-control" placeholder="Masukkan Kuantitas Barang" required="required">
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6 text-center">
                            <label class="form-control-label">Gambar</label>
                            <div class="text-center">
                                <img id="u_image" src="" class="" style="width: 100%; height: 100%;" alt="avatar">
                                <div style="width: 100%;"></div>
                                <input type="file" style="display: none;" accept="image/*" id="imageedit" name="imageedit">
                                <input type="hidden" name="image_old" id="image_old">                            
                                <button type="button" style="color: blue;" onclick="editImage()">Ubah</button> 
                                <button id="removeEdit" type="button" class="remove-hidden" onclick="removeImageEdit()" style="color: red;">| Semula</button>
                                <input type="hidden" style="display: none" value="0" name="remove" id="remove">

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer col-md-12 pull-right">
                        <button class="btn btn-outline-danger" data-dismiss="modal">Batalkan Perubahan</button>
                        <input type="reset" name="reset" class="btn btn-outline-warning" value="Reset">
                        <button type="submit" class="btn btn-outline-primary">Simpan Perubahan</button>
                    </div>
                </div>
            </form>
        </div>{{-- akhir modal body edit --}}
    </div>{{-- akhir div content edit --}} 
</div>{{-- modal-dialog modal-lg  --}}                                            
</div>{{-- modal edit --}}


<script type="text/javascript"> 
// Kembali ke Awal saat Batal Add
function resetForm() {
    $('#resetInput').click();
}
// Create Image JS
function inputImage() {
    $('#image').click();
}
$('#image').change(function () {
    var imgPath = $(this)[0].value;
    var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg") {
        var oFile = document.getElementById("image").files[0];
            if (oFile.size > 1000000) { // 1 mb for bytes.            
                alert("File size must under 1mb!");
                $('#image').val("");                          
            }
            readInputURL(this);
        }
        else {
            alert("Please select image file (jpg, jpeg, png).");
            $('#image').val("");
        }
        
    });
function readInputURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[0]);
        reader.onload = function (e) {
            $('#preview').attr('src', e.target.result);
            $('#remove').val(0);
            $('#removeAdd').removeClass('remove-hidden');
            $('#removeAdd').addClass('remove-show');
        }
    }
}
function removeImage() {
    $('#preview').attr('src', '{{ asset('img/no-image.jpg') }}');
    $('#remove').val(1);
    $('#removeAdd').removeClass('remove-show');
    $('#removeAdd').addClass('remove-hidden');
}

// Input Only Number
function isNumberKey(evt) {
 var charCode = (evt.which) ? evt.which : event.keyCode
 if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

return true;
}

// Delete Notif
$("#FormDeleteTime").submit(function (event) {
   var x = confirm("Anda yakin ingin menghapusnya?");
   if (x) {
    return true;
}
else {

    event.preventDefault();
    return false;
}

});

// Edit Image JS
function editImage() {        
    $('#imageedit').click();
}
$('#imageedit').change(function () {
    var imgPath = $(this)[0].value;
    var ext = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (ext == "gif" || ext == "png" || ext == "jpg" || ext == "jpeg") {
        var oFile = document.getElementById("imageedit").files[0];
             if (oFile.size > 1000000) { // 2 mb for bytes.            
                alert("Ukuran file harus di bawah 1 MB!");
                $('#imageedit').val("");                          
            }
            readEditURL(this);;
        }
        else {
            alert("Silakan pilih file gambar (jpg, jpeg, png).");
            $('#imageedit').val("");
            
        }

    });

function readEditURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[0]);
        reader.onload = function (e) {
            $('#u_image').attr('src', e.target.result);
            $('#remove').val(0);
            $('#removeEdit').removeClass('remove-hidden');
            $('#removeEdit').addClass('remove-show');
        }
    }
}
function removeImageEdit() {        
    $('#u_image').attr('src', '' + $('#image_old').val());
    $('#remove').val(1);
    $('#removeEdit').removeClass('remove-show');
    $('#removeEdit').addClass('remove-hidden');
}


$('#edit').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)
    var nama = button.data('nama')
    var image = button.data('image')
    var deskripsi = button.data('deskripsi')
    var code = button.data('code')
    var qty = button.data('qty')
    var item = button.data('id')
    

    var modal = $(this)

    modal.find('.modal-body #u_nama').val(nama)
    modal.find('.modal-body #u_image').attr('src', '' + image)
    modal.find('.modal-body #image_old').val(image)    
    modal.find('.modal-body #u_deskripsi').val(deskripsi)
    modal.find('.modal-body #u_code').val(code)
    modal.find('.modal-body #u_qty').val(qty)
    modal.find('.modal-body #item_id').val(item)        
});




$(document).ready(function () {


    $('#master').on('click', function(e) {
     if($(this).is(':checked',true))
     {
        $(".sub_chk").prop('checked', true);
    } else {
        $(".sub_chk").prop('checked',false);
    }
});


    $('.delete_all').on('click', function(e) {


        var allVals = [];
        $(".sub_chk:checked").each(function() {
            allVals.push($(this).attr('data-id'));
        });


        if(allVals.length <= 0 )
        {
            alert("Silakan pilih baris.");
        }  else {


            var check = confirm("Anda yakin ingin menghapus baris ini?");
            if(check == true){


                var join_selected_values = allVals.join(",");


                $.ajax({
                    url: $(this).data('url'),
                    type: 'DELETE',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: 'ids='+join_selected_values,
                    success: function (data) {
                        if (data['success']) {
                            $(".sub_chk:checked").each(function() {
                                $(this).parents("tr").remove();
                            });
                            alert(data['success']);
                        } else if (data['error']) {
                            alert(data['error']);
                        } else {
                            alert('Aduh, ada yang salah !!');
                        }
                    },
                    error: function (data) {
                        alert(data.responseText);
                    }
                });


                $.each(allVals, function( index, value ) {
                  $('table tr').filter("[data-row-id='" + value + "']").remove();
              });
            }
        }
    });



});
</script>

@endsection